﻿using Core.Entities.Reservations;

namespace Core.Entities.Extensions
{
    public static class ReservationExtensions
    {
        public static bool ReservationIsOverlaps(this Reservation existsReservation, Reservation reservationToCheck)
        {
            return existsReservation.Start <= reservationToCheck.Start &&
                   reservationToCheck.Start < existsReservation.End &&
                    existsReservation.End > reservationToCheck.End &&
                    reservationToCheck.End > existsReservation.Start
                || existsReservation.Start < reservationToCheck.Start &&
                   reservationToCheck.Start < existsReservation.End &&
                   existsReservation.End >= reservationToCheck.End &&
                   reservationToCheck.End > existsReservation.Start;
        }

        public static bool ReservationIsSamePeriod(this Reservation existsReservation, Reservation reservationToCheck)
        {
            return existsReservation.Start == reservationToCheck.Start && reservationToCheck.End == existsReservation.End;
        }

        public static bool ReservationIsIntersect(this Reservation existsReservation, Reservation reservationToCheck)
        {
            return (existsReservation.Start > reservationToCheck.Start && existsReservation.Start < reservationToCheck.End)
                   || (existsReservation.Start < reservationToCheck.Start && existsReservation.End > reservationToCheck.Start
                                                                          && existsReservation.End < reservationToCheck.End);
        }
    }
}