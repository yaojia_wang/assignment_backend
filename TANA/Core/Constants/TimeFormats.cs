﻿namespace Core.Constants
{
    public static class TimeFormats
    {
        public const string TimeSpanFormat = "HH:mm";
    }
}