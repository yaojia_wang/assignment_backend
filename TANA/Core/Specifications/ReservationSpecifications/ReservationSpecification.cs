﻿using Core.Entities.Reservations;
using System;

namespace Core.Specifications.ReservationSpecifications
{
    public sealed class ReservationSpecification : BaseSpecification<Reservation>
    {
        /// <summary>
        /// Get daily reservation.
        /// </summary>
        /// <param name="date"></param>
        public ReservationSpecification(DateTime date) : base(re => re.Date.Date == date)
        {
        }
    }
}