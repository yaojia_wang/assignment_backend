﻿using Core.Specifications.ReservationSpecifications;
using Infrastructure.Data.Repositories.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Extensions;
using Web.Interfaces;
using Web.Models.ReservationViewModels;

namespace Web.Helpers
{
    public class ReservationViewModelHelper : IReservationViewModelHelper
    {
        private readonly IReservationRepository _reservationRepository;

        public ReservationViewModelHelper(IReservationRepository reservationRepository)
        {
            _reservationRepository = reservationRepository;
        }

        public async Task<ReservationViewModel> AddReservation(CreateReservationViewModel model)
        {
            var reservation = model.ToReservation();
            await _reservationRepository.SaveReservation(reservation);
            var reservationViewModel = new ReservationViewModel(reservation);

            return reservationViewModel;
        }

        public async Task<ICollection<string>> AddReservations(IList<CreateReservationViewModel> models)
        {
            var reservations = from r in models select r.ToReservation();

            return await _reservationRepository.SaveReservations(reservations.ToList());
        }

        public async Task<IEnumerable<ReservationViewModel>> ListTodayReservations()
        {
            var spec = new ReservationSpecification(DateTime.Today);
            var reservations = await _reservationRepository.GetReservationsByCriteria(spec);
            var reservationViewModels = reservations.Select(rs => new ReservationViewModel(rs));

            return reservationViewModels;
        }
    }
}