﻿using Core.Constants;
using Core.Entities.Reservations;
using System.Collections.Generic;
using Web.Models.ReservationViewModels;

namespace Web.Extensions
{
    public static class CreateReservationViewModelExtension
    {
        public static IEnumerable<string> Validate(this CreateReservationViewModel model)
        {
            if (model.Email.IsNotValidEmail())
            {
                yield return ErrorMessages.MissingEmailAddress;
            }

            if (model.Name.IsNullOrEmpty())
            {
                yield return ErrorMessages.ContactNameRequired;
            }

            if (model.PhoneNumber.IsNullOrEmpty())
            {
                yield return ErrorMessages.ContactPhoneNumberRequired;
            }

            if (model.EndTime.IsNotValidTimeFormat())
            {
                yield return $"{ErrorMessages.ProvideValidTimeText} {nameof(model.EndTime)}{Chars.DashAsString}{TimeFormats.TimeSpanFormat}";
            }

            if (model.StartTime.IsNotValidTimeFormat())
            {
                yield return $"{ErrorMessages.ProvideValidTimeText} {nameof(model.StartTime)}{Chars.DashAsString}{TimeFormats.TimeSpanFormat}";
            }

            if (model.StartTime.IsValidTimeFormat() && model.EndTime.IsValidTimeFormat())
            {
                var startTime = model.StartTime.ToTimeSpan();
                var endTime = model.EndTime.ToTimeSpan();

                if (endTime.GetTimeSpanDifference(startTime).Ticks <= 0)
                {
                    yield return ErrorMessages.EndTimeBeforeStartTimeText;
                }
            }
        }

        public static Reservation ToReservation(this CreateReservationViewModel model)
        {
            var contactInfo = new ContactInfo(model.Email, model.Name, model.PhoneNumber);
            var reservation = new Reservation(contactInfo, model.StartTime.ToTimeSpan(), model.EndTime.ToTimeSpan());

            return reservation;
        }
    }
}