﻿using System.Collections.Generic;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;

namespace Web.Extensions
{
    public static class StringExtensions
    {
        private const string YareRegex = @"^[12][0-9]{3}$";

        public static bool IsNullOrEmpty(this string value)
        {
            return string.IsNullOrEmpty(value);
        }

        public static bool IsNotValidEmail(this string value)
        {
            try
            {
                var address = new MailAddress(value);
                return address.Address != value;
            }
            catch
            {
                return true;
            }
        }

        public static string ConvertToString<T>(this IEnumerable<T> list, string separator = ", ")
        {
            if (list == null) return string.Empty;
            var convertedItems = new StringBuilder();
            var currentSeparator = string.Empty;
            foreach (var item in list)
            {
                var itemAsString = item.ToString();
                if (itemAsString.IsNullOrEmpty()) continue;
                convertedItems.Append($"{currentSeparator}{itemAsString}");
                currentSeparator = separator;
            }

            return convertedItems.ToString();
        }

        public static bool IsValidYear(this string year, out int yearNumber)
        {
            yearNumber = 0;

            var regex = new Regex(YareRegex, RegexOptions.Compiled);
            var match = regex.IsMatch(year);

            if (match)
            {
                yearNumber = int.Parse(year);

                return true;
            }

            return false;
        }
    }
}