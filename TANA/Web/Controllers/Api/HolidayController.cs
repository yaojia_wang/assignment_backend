﻿using Infrastructure.Responses;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;
using System.Threading.Tasks;
using Web.Extensions;
using Web.Interfaces;

namespace Web.Controllers.Api
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class HolidayController : ControllerBase
    {
        private readonly IHolidayService _holidayService;
        private const string NotAValidYear = @"Provided Year Is Not a Valid Year";
        private const string EasterDays = @"Easter Days";
        private const int MaxYear = 2999;

        public HolidayController(IHolidayService holidayService)
        {
            _holidayService = holidayService;
        }

        /// <summary>
        /// Return Easter from specify year to specify interval
        /// </summary>
        /// <param name="year">The specify year, default as this year.</param>
        /// <param name="interval">The interval, default as 100.</param>
        [Route("easter")]
        [HttpGet]
        [ActionName("GetEasterDays")]
        [ProducesResponseType(typeof(ApiResponse), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ApiResponse), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetEasterDays([FromQuery]string year, int interval = 100)
        {
            int specifyYear;
            if (string.IsNullOrEmpty(year))
            {
                specifyYear = DateTime.Now.Year;
            }
            else
            {
                var isYearValid = year.IsValidYear(out specifyYear);
                if (!isYearValid)
                {
                    return BadRequest(new ApiResponse(NotAValidYear, $"{NotAValidYear}: {year}"));
                }
            }

            if (specifyYear + interval > MaxYear)
            {
                interval = MaxYear - specifyYear;
            }

            var easterDays = await _holidayService.EasterSundays(specifyYear, interval);

            return Ok(new ApiResponse(true, EasterDays, easterDays));
        }
    }
}