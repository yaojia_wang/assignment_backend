﻿using Infrastructure.Responses;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Threading.Tasks;
using Web.Interfaces;

namespace Web.Controllers.Api
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class FibonacciController : ControllerBase
    {
        private readonly IFibonacciService _fibonacciService;
        private const string FibonacciNumberChecked = @"Fibonacci Number Checked";

        public FibonacciController(IFibonacciService fibonacciService)
        {
            _fibonacciService = fibonacciService;
        }

        /// <summary>
        /// Check if the provided number is Fibonacci number
        /// </summary>
        /// <param name="number">Provided number</param>
        [Route("check")]
        [HttpGet]
        [ActionName("AddReservation")]
        [ProducesResponseType(typeof(ApiResponse), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ApiResponse), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> IsFibonacciNumber([FromQuery][Required]ulong number)
        {
            var result = await _fibonacciService.IsFibonacciNumberAsync(number);

            return Ok(new ApiResponse(true, FibonacciNumberChecked, result));
        }
    }
}