﻿using Infrastructure.Responses;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Web.Extensions;
using Web.Interfaces;
using Web.Models.ReservationViewModels;

namespace Web.Controllers.Api
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class ReservationsController : ControllerBase
    {
        private readonly IReservationViewModelHelper _reservationViewModelHelper;
        private const string ListOfTodayReservation = @"List of today's reservations";
        private const string ReservationAdded = @"Reservation added successfully";
        private const string ModelValidationError = @"Model Validation Error";
        private const string FileUploadError = @"File Upload Error";
        private const string FileNoData = @"File Contain No Data";
        private const string UploadSuccess = @"File Upload Success";
        private const string UploadWithSkippedItems = @"File Upload With Skipped Items";

        public ReservationsController(IReservationViewModelHelper reservationViewModelHelper)
        {
            _reservationViewModelHelper = reservationViewModelHelper;
        }

        /// <summary>
        /// List today reservations
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ActionName("ListAll")]
        [ProducesResponseType(typeof(ApiResponse), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> ListAll()
        {
            var reservationViewModels = await _reservationViewModelHelper.ListTodayReservations();

            return Ok(new ApiResponse(true, ListOfTodayReservation, reservationViewModels));
        }

        /// <summary>
        /// Add New Reservation -timeFormat: HH:mm - ex 18:30
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("AddReservation")]
        [ProducesResponseType(typeof(ApiResponse), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ApiResponse), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> AddReservation([FromBody]CreateReservationViewModel model)
        {
            var modelValidation = model.Validate().ToList();
            if (modelValidation.Any())
            {
                return BadRequest(new ApiResponse(ModelValidationError, modelValidation.ConvertToString()));
            }
            var reservationViewModel = await _reservationViewModelHelper.AddReservation(model);

            return Ok(new ApiResponse(true, ReservationAdded, reservationViewModel));
        }

        /// <summary>
        /// Upload reservation by a Json file.
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [Route("upload")]
        [HttpPost]
        [ActionName("UploadFile")]
        [ProducesResponseType(typeof(ApiResponse), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ApiResponse), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UploadFile([FromForm(Name = "file")]IFormFile file)
        {
            try
            {
                // Normally we should check if the type of the file is exact type as we want,
                // there are some way to do that, for example check the first byte of the file,
                // due to time limit in this test, just skip this part. and just simply check
                // The file name.
                var filename = file.FileName;
                if (file.Length <= 0 && filename.Split(".")[1].ToLower() != "json")
                {
                    return Ok(new ApiResponse(true, FileNoData));
                }

                string data;

                using (var reader = new StreamReader(file.OpenReadStream()))
                {
                    data = reader.ReadToEnd();
                }

                var reservations = JsonConvert.DeserializeObject<List<CreateReservationViewModel>>(data);
                var messages = new List<string>();

                foreach (var reservation in reservations)
                {
                    var modelValidation = reservation.Validate().ToList();

                    if (!modelValidation.Any())
                    {
                        continue;
                    }
                    messages.Add(modelValidation.ConvertToString());
                    reservations.Remove(reservation);
                }

                var uploadMessage = await _reservationViewModelHelper.AddReservations(reservations);
                messages.AddRange(uploadMessage);

                return Ok(messages.Any() ?
                    new ApiResponse(true, $"{UploadWithSkippedItems}: {string.Join(',', messages)}") :
                    new ApiResponse(true, UploadSuccess));
            }
            catch (System.Exception ex)
            {
                return BadRequest(new ApiResponse(FileUploadError, ex.Message));
            }
        }
    }
}