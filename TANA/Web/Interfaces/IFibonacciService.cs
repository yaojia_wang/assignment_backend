﻿using System.Threading.Tasks;

namespace Web.Interfaces
{
    public interface IFibonacciService
    {
        Task<bool> IsFibonacciNumberAsync(ulong number);
    }
}