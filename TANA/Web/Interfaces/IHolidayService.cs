﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Web.Interfaces
{
    public interface IHolidayService
    {
        Task<IList<DateTime>> EasterSundays(int year, int interval);
    }
}