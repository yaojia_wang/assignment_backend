﻿using System;
using System.Threading.Tasks;
using Web.Interfaces;

namespace Web.Services
{
    public class FibonacciService : IFibonacciService
    {
        public async Task<bool> IsFibonacciNumberAsync(ulong number)
        {
                        
            return await Task.Run(() => IsFibonacciCalculateWithFormula(number));
        }

        private static bool IsPerfectSquare(ulong x)
        {
            var s = (ulong)Math.Sqrt(x);
            return (s * s == x);
        }

        private static bool IsFibonacciCalculateWithFormula(ulong number)
        {
            // n is Fibonacci if one of
            // 5*n*n + 4 or 5*n*n - 4 or
            // both are a perfect square
            var result = IsPerfectSquare(checked(5 * number * number + 4)) ||
                  IsPerfectSquare(checked(5 * number * number - 4));

            return result;
        }      
    }
}