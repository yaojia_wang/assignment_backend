﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Web.Interfaces;

namespace Web.Services
{
    public class HolidayService : IHolidayService
    {
        public async Task<IList<DateTime>> EasterSundays(int year, int interval)
        {
            var tasks = new List<Task<DateTime>>();

            for (var i = 0; i <= interval; i++)
            {
                tasks.Add(EasterSunday(year));
                ++year;
            }

            return await Task.WhenAll(tasks);
        }

        private static async Task<DateTime> EasterSunday(int year)
        {
            return await Task.Run(() =>
            {
                var day = 0;
                var month = 0;

                var g = year % 19;
                var c = year / 100;
                var h = (c - c / 4 - (8 * c + 13) / 25 + 19 * g + 15) % 30;
                var i = h - h / 28 * (1 - h / 28 * (29 / (h + 1)) * ((21 - g) / 11));

                day = i - (year + year / 4 + i + 2 - c + c / 4) % 7 + 28;
                month = 3;

                if (day > 31)
                {
                    month++;
                    day -= 31;
                }

                return new DateTime(year, month, day);
            });
        }
    }
}