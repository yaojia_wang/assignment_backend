using AutoFixture;
using Core.Entities.Extensions;
using Core.Entities.Reservations;
using System;
using System.Collections.Generic;
using Xunit;

namespace Test
{
    public class ReservationExtensionsTest
    {
        private readonly Fixture _fixture = new Fixture();
        private static Reservation _existReservation;
        private static ContactInfo _contactInfo;

        private const string Overlaps = @"Overlaps";
        private const string SamePeriod = @"The Same Period";
        private const string Intersect = @"Intersect";

        public ReservationExtensionsTest()
        {
            _contactInfo = _fixture.Create<ContactInfo>();
            _existReservation = new Reservation(_contactInfo,
                TimeSpan.Parse("10:00"), TimeSpan.Parse("11:00"));
        }

        [Theory]
        [MemberData(nameof(GetTestData))]
        public void ReservationIsIntersect_Test(Reservation reservationToCheck, Dictionary<string, bool> expected)
        {
            Assert.Equal(expected[Intersect], _existReservation.ReservationIsIntersect(reservationToCheck));
        }

        [Theory]
        [MemberData(nameof(GetTestData))]
        public void ReservationIsTheSame_Test(Reservation reservationToCheck, Dictionary<string, bool> expected)
        {
            Assert.Equal(expected[SamePeriod], _existReservation.ReservationIsSamePeriod(reservationToCheck));
        }

        [Theory]
        [MemberData(nameof(GetTestData))]
        public void ReservationIsOverlaps_Test(Reservation reservationToCheck, Dictionary<string, bool> expected)
        {
            Assert.Equal(expected[Overlaps], _existReservation.ReservationIsOverlaps(reservationToCheck));
        }

        private static Reservation GenerateReservation(TimeSpan start, TimeSpan end)
        {
            return new Reservation(_contactInfo, start, end);
        }

        public static IEnumerable<object[]> GetTestData()
        {
            var left = new List<TimeSpan>
            {
                TimeSpan.Parse("08:00"),
                TimeSpan.Parse("09:00"),
            };

            var right = new List<TimeSpan>
            {
                TimeSpan.Parse("12:00"),
                TimeSpan.Parse("13:00"),
            };

            var between = new List<TimeSpan>
            {
                TimeSpan.Parse("10:10"),
                TimeSpan.Parse("10:30"),
            };

            // Earlier
            yield return new object[] { GenerateReservation(left[0], left[1]), new Dictionary<string, bool> { { Intersect, false }, { Overlaps, false }, { SamePeriod, false } } };

            // Later
            yield return new object[] { GenerateReservation(right[0], right[1]), new Dictionary<string, bool> { { Intersect, false }, { Overlaps, false }, { SamePeriod, false } } };

            // The same
            yield return new object[] { GenerateReservation(TimeSpan.Parse("10:00"), TimeSpan.Parse("11:00")), new Dictionary<string, bool> { { Intersect, false }, { Overlaps, false }, { SamePeriod, true } } };

            // Start and end between exist reservation.
            yield return new object[] { GenerateReservation(between[0], between[1]), new Dictionary<string, bool> { { Intersect, false }, { Overlaps, true }, { SamePeriod, false } } };

            // Start the same as exist reservation but end before exists reservation
            yield return new object[] { GenerateReservation(TimeSpan.Parse("10:00"), TimeSpan.Parse("10:30")), new Dictionary<string, bool> { { Intersect, false }, { Overlaps, true }, { SamePeriod, false } } };

            // Start the after exist reservation but end the same exists reservation
            yield return new object[] { GenerateReservation(TimeSpan.Parse("10:30"), TimeSpan.Parse("11:00")), new Dictionary<string, bool> { { Intersect, false }, { Overlaps, true }, { SamePeriod, false } } };

            // Before exists reservation start and end between exists reservation.
            yield return new object[] { GenerateReservation(TimeSpan.Parse("08:00"), TimeSpan.Parse("10:30")), new Dictionary<string, bool> { { Intersect, true }, { Overlaps, false }, { SamePeriod, false } } };

            // Start between exists reservation and end after exists reservation.
            yield return new object[] { GenerateReservation(TimeSpan.Parse("10:10"), TimeSpan.Parse("11:30")), new Dictionary<string, bool> { { Intersect, true }, { Overlaps, false }, { SamePeriod, false } } };

            // Start before exists reservation and end after exists reservation.
            yield return new object[] { GenerateReservation(TimeSpan.Parse("08:00"), TimeSpan.Parse("11:30")), new Dictionary<string, bool> { { Intersect, true }, { Overlaps, false }, { SamePeriod, false } } };
        }
    }
}