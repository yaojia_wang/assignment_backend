﻿using System.Data;

namespace Infrastructure.Data.Interfaces
{
    /// <summary>
    ///     Contract for the unit of work service.
    /// </summary>
    public interface IUnitOfWorkService
    {
        /// <summary>
        ///     Creates a new unit of work.
        /// </summary>
        /// <param name="repositories">The repositories that will be used.</param>
        /// <returns>The created unit of work.</returns>
        IUnitOfWork CreateUnitOfWork(params IUnitOfWorkFactory[] repositories);

        /// <summary>
        ///     Creates a new unit of work with a specific isolation level.
        /// </summary>
        /// <param name="isolationLevel">The isolation level to use.</param>
        /// <param name="repositories">The repositories that will be used.</param>
        /// <returns>The created unit of work.</returns>
        IUnitOfWork CreateUnitOfWork(IsolationLevel isolationLevel, params IUnitOfWorkFactory[] repositories);

        /// <summary>
        ///     Creates a new read-only unit of work.
        /// </summary>
        /// <param name="repositories">The repositories that will be used.</param>
        /// <returns>The created unit of work.</returns>
        IUnitOfWorkReadOnly CreateReadOnlyUnitOfWork(params IUnitOfWorkFactory[] repositories);
    }
}