﻿namespace Infrastructure.Data.Interfaces
{
    /// <summary>
    ///     Contract for a unit of work locactor.
    /// </summary>
    public interface IAmbientUnitOfWorkLocator
    {
        /// <summary>
        ///     Gets an ambient unit of work.
        /// </summary>
        /// <returns>The unit of work.</returns>
        IUnitOfWork GetAmbientUnitOfWork();
    }
}