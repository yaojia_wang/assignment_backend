﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace Infrastructure.Data.Interfaces
{
    /// <summary>
    ///     Contract for a generic repository.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public interface IRepository<TEntity> : IUnitOfWorkFactory where TEntity : class
    {
        #region Retrieval Methods

        /// <summary>
        ///     Retrieves Entity using identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The Entity</returns>
        TEntity FindById(object id);

        /// <summary>
        ///     Finds the specified filter.
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <returns>Retrieved entities.</returns>
        IQueryable<TEntity> Find(Expression<Func<TEntity, bool>> filter);

        /// <summary>
        ///     Gets all entities in a repository.
        /// </summary>
        /// <returns>Retrieved entities.</returns>
        IQueryable<TEntity> Get();

        #endregion Retrieval Methods

        #region Modification Methods

        /// <summary>
        ///     Returns a new instance of the Model.
        /// </summary>
        /// <returns>A new instance</returns>
        TEntity Create();

        /// <summary>
        ///     Adds an entity object to the repository.
        /// </summary>
        /// <param name="entity">Entity to be added.</param>
        void Add(TEntity entity);

        /// <summary>
        ///     Deletes an entity object from repository through Id.
        /// </summary>
        /// <param name="id">Unique entity identifier.</param>
        void Delete(object id);

        /// <summary>
        ///     Marks the tracked entity as updated within the repository.
        /// </summary>
        /// <param name="entityToUpdate">Tracked entity with changes.</param>
        void Update(TEntity entityToUpdate);

        #endregion Modification Methods
    }
}