﻿using System;

namespace Infrastructure.Data.Interfaces
{
    /// <summary>
    ///     Readonly unit of work contract.
    /// </summary>
    public interface IUnitOfWorkReadOnly : IDisposable
    {
    }
}