﻿using System.Data;

namespace Infrastructure.Data.Interfaces
{
    /// <summary>
    ///     Unit of work factory contract.
    /// </summary>
    public interface IUnitOfWorkFactory
    {
        /// <summary>
        ///     Creates a new unit of work.
        /// </summary>
        /// <returns>The created unit of work.</returns>
        IUnitOfWork CreateUnitOfWork();

        /// <summary>
        ///     Creates a new unit of work with a specific isolation level.
        /// </summary>
        /// <param name="isolationLevel">The isolation level to use.</param>
        /// <returns>The created unit of work.</returns>
        IUnitOfWork CreateUnitOfWork(IsolationLevel isolationLevel);

        /// <summary>
        ///     Creates a new read-only unit of work.
        /// </summary>
        /// <returns>The created unit of work.</returns>
        IUnitOfWorkReadOnly CreateReadOnlyUnitOfWork();
    }
}