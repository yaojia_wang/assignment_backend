﻿using System;

namespace Infrastructure.Data.Interfaces
{
    /// <summary>
    ///     Unit of work contract.
    /// </summary>
    public interface IUnitOfWork : IDisposable
    {
        /// <summary>
        ///     Commits the operations. Changes are written to the database.
        /// </summary>
        void Commit();

        /// <summary>
        ///     Rolls back the operations. Changes are discarded.
        /// </summary>
        void Rollback();
    }
}