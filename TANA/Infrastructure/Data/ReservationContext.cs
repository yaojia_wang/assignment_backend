﻿using Core.Entities.Reservations;
using Infrastructure.Data.EntityFramework;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Data
{
    public class ReservationContext : DbContextBase
    {
        public virtual DbSet<Reservation> Reservations { get; set; }

        public ReservationContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Reservation>(ConfigureReservation);
        }

        private void ConfigureReservation(EntityTypeBuilder<Reservation> builder)
        {
            builder.ToTable("Reservations");
            builder.HasKey(re => re.Id);
            builder.OwnsOne(re => re.ContactInfo);
            builder.Property(re => re.Start).
                IsRequired(true);
            builder.Property(re => re.End).
                IsRequired(true);
            builder.HasIndex(re => new { re.Start, re.End, re.Date }).IsUnique(true);
        }
    }
}