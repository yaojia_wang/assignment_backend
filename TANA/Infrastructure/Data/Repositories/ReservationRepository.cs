﻿using Core.Entities.Extensions;
using Core.Entities.Reservations;
using Core.Interfaces;
using Core.Specifications.ReservationSpecifications;
using Infrastructure.Data.EntityFramework;
using Infrastructure.Data.Repositories.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Data.Repositories
{    
    public class ReservationRepository : Repository<ReservationContext, Reservation>, IReservationRepository
    {
        public ReservationRepository(string connectionString) : base(connectionString)
        {
        }

        private const string Overlaps = @"Overlaps";
        private const string SamePeriod = @"The Same Period";
        private const string Intersect = @"Intersect";
        private const string NoConflict = @"No Conflict";

        public Task SaveReservation(Reservation reservation, ISpecification<Reservation> specification = null)
        {
            return Task.Run(() =>
            {
                var message = InsertReservation(reservation, specification);

                if (!string.IsNullOrEmpty(message))
                {
                    throw new ArgumentException(message);
                }
            });
        }

        public Task<ICollection<string>> SaveReservations(IList<Reservation> reservations, ISpecification<Reservation> specification = null)
        {
            return Task.Run<ICollection<string>>(() =>
            {
                return reservations.Select(reservation => InsertReservation(reservation, specification))
                    .Where(message => !string.IsNullOrEmpty(message)).ToList();
            });
        }

        public Task<IEnumerable<Reservation>> GetReservationsByCriteria(ISpecification<Reservation> spec)
        {
            return Task.Run<IEnumerable<Reservation>>(() =>
            {
                using (CreateUnitOfWork())
                {
                    return Find(spec.Criteria).ToList();
                }
            });
        }

        private static string ValidateReservationDate(IList<Reservation> existsReservations, Reservation reservationToCheck)
        {
            if (!existsReservations.Any())
            {
                return string.Empty;
            }

            var conflictList = from e in existsReservations
                               let range = (e.ReservationIsOverlaps(reservationToCheck) ? Overlaps :
                                   e.ReservationIsSamePeriod(reservationToCheck) ? SamePeriod :
                                   e.ReservationIsIntersect(reservationToCheck) ? Intersect :
                                   NoConflict)
                               group e by range into g
                               where g.Key != NoConflict
                               select $"Reservation event from {reservationToCheck.Start} to {reservationToCheck.End} is {g.Key} with " +
                                      $"reservation Id: {string.Join(',', g.Select(r => r.Id.ToString()))}";

            var message = string.Join(Environment.NewLine, conflictList);

            return message;
        }

        private string InsertReservation(Reservation reservation, ISpecification<Reservation> specification = null)
        {
            using (var uow = CreateUnitOfWork())
            {
                try
                {
                    specification = specification ?? new ReservationSpecification(reservation.Date);
                    var existsReservation = Find(specification.Criteria).ToList();

                    var message = ValidateReservationDate(existsReservation, reservation);

                    if (!string.IsNullOrEmpty(message))
                    {
                        return message;
                    }

                    Add(reservation);
                    uow.Commit();

                    return message;
                }
                catch
                {
                    uow.Rollback();
                    throw;
                }
            }
        }
    }
}