﻿using Core.Entities.Reservations;
using Core.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Infrastructure.Data.Repositories.Interface
{
    public interface IReservationRepository
    {
        Task SaveReservation(Reservation reservation, ISpecification<Reservation> spec = null);

        Task<ICollection<string>> SaveReservations(IList<Reservation> reservations,
            ISpecification<Reservation> specification = null);

        Task<IEnumerable<Reservation>> GetReservationsByCriteria(ISpecification<Reservation> spec);
    }
}