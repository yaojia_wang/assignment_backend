﻿using Infrastructure.Data.EntityFramework.Interfaces;
using System;
using System.Diagnostics;

namespace Infrastructure.Data.EntityFramework.States
{
    public class ChildState : IParentageState
    {
        public void Commit(DbContextCollection dbContextCollection)
        {
        }

        public void Dispose(ref bool isCompleted, bool readOnly, DbContextCollection dbContexts)
        {
        }

        public void SetAmbientUnitOfWork(UnitOfWork parentUnitOfWork, bool? parentIsDisposed)
        {
            if (parentIsDisposed.HasValue && parentIsDisposed.Value)
            {
                var message =
                    @"PROGRAMMING ERROR - When attempting to dispose a DbContextScope, we found that our parent DbContextScope has already been disposed! This means that someone started a parallel flow of execution (e.g. created a TPL task, created a thread or enqueued a work item on the ThreadPool) within the context of a DbContextScope without suppressing the ambient context first.
                                In order to fix this:
                                1) Look at the stack trace below - this is the stack trace of the parallel task in question.
                                2) Find out where this parallel task was created.
                                3) Change the code so that the ambient context is suppressed before the parallel task is created. You can do this with IDbContextScopeFactory.SuppressAmbientContext() (wrap the parallel task creation code block in this).
                                Stack Trace:
                                " + Environment.StackTrace;

                Debug.WriteLine(message);
            }
            else
            {
                UnitOfWork.SetAmbientUnitOfWork(parentUnitOfWork);
            }
        }
    }
}