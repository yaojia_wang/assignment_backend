﻿using Infrastructure.Data.EntityFramework.Interfaces;
using System;

namespace Infrastructure.Data.EntityFramework.States
{
    public class ParentState : IParentageState
    {
        public void Commit(DbContextCollection dbContextCollection)
        {
            dbContextCollection.Commit();
        }

        public void Dispose(ref bool isCompleted, bool readOnly, DbContextCollection dbContexts)
        {
            // Commit / Rollback and dispose all of our DbContext instances.
            if (!isCompleted)
            {
                // Do our best to clean up as much as we can but don't throw here as it's too late anyway.
                try
                {
                    if (readOnly)
                    {
                        // Disposing a read-only unit of work before having called the Commit() method
                        // is the normal and expected behavior. Read-only scopes get committed automatically.
                        dbContexts.Commit();
                    }
                    else
                    {
                        // Disposing a read/write unit of work before having called the Commit() method
                        // indicates that something went wrong and that all changes should be rolled-back.
                        dbContexts.Rollback();
                    }
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception.Message);
                }

                isCompleted = true;
            }

            dbContexts.Dispose();
        }

        public void SetAmbientUnitOfWork(UnitOfWork parentUnitOfWork, bool? parentIsDisposed)
        {
            //This does not apply to the ParentState
        }
    }
}