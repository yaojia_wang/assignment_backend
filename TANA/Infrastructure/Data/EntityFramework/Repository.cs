﻿using Infrastructure.Data.EntityFramework.Interfaces;
using Infrastructure.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Infrastructure.Data.EntityFramework
{
    /// <summary>
    ///     Base repository which exposes CRUD functionality for an EfCore DbSet.
    /// </summary>
    /// <typeparam name="TDbContext">The type of DbContext.</typeparam>
    /// <typeparam name="TEntity">The entity type which this repository does CRUD operations on.</typeparam>
    public abstract class Repository<TDbContext, TEntity> : IEfRepository<TEntity>, IRepository<TEntity>
        where TDbContext : DbContextBase where TEntity : class
    {
        /// <summary>
        ///     Connection string used to create instances of DbContext, and identify repositories.
        /// </summary>
        public string ConnectionString { get; }

        private DbSet<TEntity> DbSet => DbContext.Set<TEntity>();

        private TDbContext DbContext
        {
            get
            {
                var ambientEfUnitOfWork = GetAmbientUnitOfWork() as UnitOfWork;

                if (ambientEfUnitOfWork == null)
                {
                    throw new InvalidOperationException("No ambient Entity Framework Unit Of Work found.");
                }

                var dbContext = ambientEfUnitOfWork.DbContexts.Get<TDbContext>(ConnectionString);

                if (dbContext == null)
                {
                    throw new InvalidOperationException(
                        $"No ambient db context of type {typeof(TDbContext).Name} found");
                }

                return dbContext as TDbContext;
            }
        }

        protected Repository(string connectionString)
        {
            ConnectionString = connectionString;
        }

        /// <summary>
        ///     Adds instance to DbSet.
        /// </summary>
        /// <param name="entity">Entity to add.</param>
        public void Add(TEntity entity)
        {
            DbSet.Add(entity);
        }

        /// <summary>
        ///     Creates an instance of type <see cref="TEntity" />.
        /// </summary>
        /// <returns>An instance of type <see cref="TEntity" />.</returns>
        public TEntity Create()
        {
            return Activator.CreateInstance<TEntity>();
        }

        /// <summary>
        ///     Creates a readonly unit of work.
        /// </summary>
        /// <returns>Readonly unit of work instance.</returns>
        public IUnitOfWorkReadOnly CreateReadOnlyUnitOfWork()
        {
            return new UnitOfWorkReadOnly();
        }

        /// <summary>
        ///     Creates a unit of work.
        /// </summary>
        /// <returns>Unit of work instance.</returns>
        public IUnitOfWork CreateUnitOfWork()
        {
            return new UnitOfWork();
        }

        /// <summary>
        ///     Creates a  unit of work with the given <paramref name="isolationLevel" />.
        /// </summary>
        /// <param name="isolationLevel"></param>
        /// <returns>Unit of work instance.</returns>
        public IUnitOfWork CreateUnitOfWork(IsolationLevel isolationLevel)
        {
            return new UnitOfWork(isolationLevel);
        }

        /// <summary>
        ///     Deletes entity from DbSet, having given <paramref name="id" />.
        /// </summary>
        /// <param name="id">The id.</param>
        public void Delete(object id)
        {
            var entityToDelete = DbSet.Find(id);
            Delete(entityToDelete);
        }

        /// <summary>
        ///     Deletes entity from DbSet.
        /// </summary>
        /// <param name="entityToDelete">The entity.</param>
        public void Delete(TEntity entityToDelete)
        {
            if (DbContext.Entry(entityToDelete).State == EntityState.Detached)
            {
                DbSet.Attach(entityToDelete);
            }
            DbSet.Remove(entityToDelete);
        }

        /// <summary>
        ///     Determines whether the specified <see cref="System.Object" />, is equal to this instance.
        /// </summary>
        /// <param name="repository">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///     <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise,
        ///     <c>false</c>.
        /// </returns>
        public override bool Equals(object repository)
        {
            var internalRepository = repository as IEfRepository<TEntity>;

            if (internalRepository == null)
            {
                return false;
            }

            if (ConnectionString.Equals(internalRepository.ConnectionString))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        ///     Finds one or more entities which satisfy given filter.
        /// </summary>
        /// <param name="filter">The filter expression.</param>
        /// <returns>Queryable collection of type <see cref="TEntity" />.</returns>
        public IQueryable<TEntity> Find(Expression<Func<TEntity, bool>> filter)
        {
            return DbSet.Where(filter);
        }

        /// <summary>
        ///     Find an entity by Id.
        /// </summary>
        /// <param name="id">The id</param>
        /// <returns>Entity of type <see cref="TEntity" />.</returns>
        public TEntity FindById(object id)
        {
            return DbSet.Find(id);
        }

        /// <summary>
        ///     Returns the unfiltered set.
        /// </summary>
        /// <returns>Unfiltered queryable collection.</returns>
        public IQueryable<TEntity> Get()
        {
            return DbSet;
        }

        /// <summary>
        ///     Returns the ambient unit of work (ie that at the top of the stack.
        /// </summary>
        /// <returns>Unit of work instance.</returns>
        public IUnitOfWork GetAmbientUnitOfWork()
        {
            return UnitOfWork.GetAmbientUnitOfWork();
        }

        /// <summary>
        ///     Updates given entity.
        /// </summary>
        /// <param name="entityToUpdate">The entity.</param>
        public void Update(TEntity entityToUpdate)
        {
            DbSet.Update(entityToUpdate);
        }

        /// <summary>
        ///     Executes query and returns <see cref="IEnumerable{TResult}"/>.
        /// </summary>
        /// <typeparam name="TResult">The generic type within the resulting list.</typeparam>
        /// <param name="query">The raw sql query.</param>
        /// <param name="parameters">Parameter list which will be applied to query.</param>
        /// <returns>Resulting <see cref="IEnumerable{TResult}"/>.</returns>
        protected virtual IEnumerable<TResult> ExecuteQuery<TResult>(string query, params object[] parameters) where TResult : TEntity
        {
            return DbSet.FromSql(query, parameters).AsEnumerable() as IEnumerable<TResult>;
        }

        protected virtual object FromSql(string query, params object[] parameters)
        {
            return DbSet.FromSql(query, parameters);
        }

        /// <summary>
        ///     Executes the given query against the database context.
        /// </summary>
        /// <param name="query">The query to execute.</param>
        /// <param name="parameters">Parameters to apply to query.</param>
        /// <returns>Number of affected records.</returns>
        protected virtual int ExecuteQuery(string query, params object[] parameters)
        {
            return DbContext.Database.ExecuteSqlCommand(query, parameters);
        }

        /// <summary>
        ///     Refreshes a change by removing the change from the object tracker.
        /// </summary>
        protected virtual void Refresh(EntityEntry entry)
        {
            switch (entry.State)
            {
                // Under the covers, changing the state of an entity from
                // Modified to Unchanged first sets the values of all
                // properties to the original values that were read from
                // the database when it was queried, and then marks the
                // entity as Unchanged. This will also reject changes to
                // FK relationships since the original value of the FK
                // will be restored.
                case EntityState.Modified:
                    entry.State = EntityState.Unchanged;
                    break;

                case EntityState.Added:
                    entry.State = EntityState.Detached;
                    break;
                // If the EntityState is the Deleted, reload the date from the database.
                case EntityState.Deleted:
                    entry.Reload();
                    break;
            }
        }

        /// <summary>
        ///     Refreshes all changes by removing any changes from the object tracker.
        /// </summary>
        protected virtual void RefreshAll()
        {
            foreach (var entry in DbContext.ChangeTracker.Entries())
            {
                Refresh(entry);
            }
        }

        /// <summary>
        ///     Refreshes all changes asynchronously. This removes any changes from the object tracker.
        /// </summary>
        /// <returns>
        ///     Task that can be awaited.
        /// </returns>
        protected virtual async Task RefreshAllAsync()
        {
            foreach (var entry in DbContext.ChangeTracker.Entries())
            {
                await RefreshAsync(entry);
            }
        }

        /// <summary>
        ///     Refreshes an enityEntry asynchronously.
        /// </summary>
        /// <param name="entry">The entry.</param>
        /// <returns>
        ///     Task that can be awaited.
        /// </returns>
        protected virtual async Task RefreshAsync(EntityEntry entry)
        {
            await Task.Factory.StartNew(() => Refresh(entry));
        }
    }
}