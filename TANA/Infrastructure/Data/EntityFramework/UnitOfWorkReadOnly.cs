﻿using Infrastructure.Data.Interfaces;
using System.Data;

namespace Infrastructure.Data.EntityFramework
{
    /// <summary>
    ///     A readonly unit of work.
    /// </summary>
    public class UnitOfWorkReadOnly : IUnitOfWorkReadOnly
    {
        private readonly UnitOfWork _internalUnitOfWork;

        internal UnitOfWorkReadOnly(IsolationLevel? isolationLevel = null)
        {
            _internalUnitOfWork = new UnitOfWork(isolationLevel, true);
        }

        /// <summary>
        ///     Disposes the resources.
        /// </summary>
        public void Dispose()
        {
            _internalUnitOfWork.Dispose();
        }
    }
}