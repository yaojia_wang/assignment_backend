﻿using Infrastructure.Data.EntityFramework.Interfaces;
using Infrastructure.Data.EntityFramework.States;
using Infrastructure.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading;

namespace Infrastructure.Data.EntityFramework
{
    /// <summary>
    ///     Implements <see cref="IUnitOfWork" /> in EfCore specific manner.
    /// </summary>
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IParentageState _parentageState;
        private readonly UnitOfWork _parentUnitOfWork;
        private readonly bool _readOnly;

        private readonly Guid Id;
        private bool _isCompleted = false;
        private bool _isDisposed = false;

        internal DbContextCollection DbContexts { get; }

        internal UnitOfWork()
            : this(null)
        {
        }

        internal UnitOfWork(IsolationLevel? isolationLevel)
            : this(isolationLevel, false)
        {
        }

        internal UnitOfWork(IsolationLevel? isolationLevel, bool readOnly)
        {
            Id = Guid.NewGuid();

            _readOnly = readOnly;
            _parentUnitOfWork = GetAmbientUnitOfWork();

            if (_parentUnitOfWork != null)
            {
                DbContexts = _parentUnitOfWork.DbContexts;
                _parentageState = new ChildState();
            }
            else
            {
                _parentageState = new ParentState();
                DbContexts = new DbContextCollection(isolationLevel);
            }

            SetAmbientUnitOfWork(this);
        }

        /// <summary>
        ///     Commits all instance of DbContexts according to the parentage state. Marks this unit of work as
        ///     Completed.
        /// </summary>
        /// <exception cref="ObjectDisposedException">When this unit of work has already been disposed.</exception>
        /// <exception cref="InvalidOperationException">When this unit of work has been marked as completed.</exception>
        public void Commit()
        {
            if (_isDisposed)
            {
                throw new ObjectDisposedException("UnitOfWork");
            }

            if (_isCompleted)
            {
                throw new InvalidOperationException("You cannot call Commit() more than once on a UnitOfWork.");
            }

            _parentageState.Commit(DbContexts);

            _isCompleted = true;
        }

        /// <summary>
        ///     Removes this unit of work.
        /// </summary>
        /// <exception cref="InvalidOperationException">If this instance is not the ambient unit of work.</exception>
        public void Dispose()
        {
            if (_isDisposed)
            {
                return;
            }

            _parentageState.Dispose(ref _isCompleted, _readOnly, DbContexts);

            // Pop this instance from the ambient scope stack.
            var currentAmbientUnitOfWork = GetAmbientUnitOfWork();
            if (currentAmbientUnitOfWork != this) // This is a serious programming error. Worth throwing here.
            {
                throw new InvalidOperationException(
                    "Transactions must be disposed of in the order in which they were created!");
            }

            RemoveAmbientUnitOfWork();

            _isDisposed = true;
        }

        /// <summary>
        ///     Rolls back all DBContexts.
        /// </summary>
        public void Rollback()
        {
            DbContexts.Rollback();
        }

        #region Ambient Unit Of Work logic

        private static readonly AsyncLocal<Stack<UnitOfWork>> CallContext = new AsyncLocal<Stack<UnitOfWork>> { Value = new Stack<UnitOfWork>() };

        /// <summary>
        ///     Makes the provided unit of work available as the the ambient unit of work via the CallContext.
        /// </summary>
        internal static void SetAmbientUnitOfWork(UnitOfWork newAmbientUnitOfWork)
        {
            if (newAmbientUnitOfWork == null)
            {
                throw new ArgumentNullException(nameof(newAmbientUnitOfWork));
            }
            CallContext.Value.Push(newAmbientUnitOfWork);
        }

        /// <summary>
        ///     Clears the ambient unit of work from the CallContext and stops tracking its instance.
        ///     Call this when a unit of work is being disposed.
        /// </summary>
        internal static void RemoveAmbientUnitOfWork()
        {
            CallContext.Value.Pop();
        }

        /// <summary>
        ///     Clears the ambient unit of work from the CallContext but keeps tracking its instance. Call this
        ///     to temporarily
        ///     hide the ambient unit of work (e.g. to prevent it from being captured by parallel task).
        /// </summary>
        internal static void HideAmbientScope()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     Get the current ambient unit of work or null if no ambient unit of work has been setup.
        /// </summary>
        internal static UnitOfWork GetAmbientUnitOfWork()
        {
            if (CallContext.Value == null)
                CallContext.Value = new Stack<UnitOfWork>();

            return CallContext.Value.Count > 0 ? CallContext.Value.Peek() : null;
        }

        #endregion Ambient Unit Of Work logic
    }
}