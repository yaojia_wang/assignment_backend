﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace Infrastructure.Data.EntityFramework
{
    public class DbContextBase : DbContext
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="DbContextBase" /> class.
        /// </summary>
        /// <param name="nameOrConnectionString">Either the database name or a connection string.</param>
        public DbContextBase(string nameOrConnectionString)
            : base(SantizeConnectionString(nameOrConnectionString))
        {
        }

        /// <summary>
        ///     <para>
        ///         Looks through the connection string and checks if it is the EDMX style connection
        ///         string like the following:
        ///     </para>
        ///     <para>
        ///         metadata=res://*/;provider=System.Data.SqlClient;provider connection string=\"data
        ///         source=[HOSTNAME];Initial Catalog=[db];User
        ///         ID=user;Password=password;MultipleActiveResultSets=True;App=EntityFramework"
        ///     </para>
        ///     If a match is found, the 'provider connection string' section is extacted and returned.
        ///     Otherwise the original connection string is returned.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <returns><see cref="DbContextOptions" /> instance.</returns>
        private static DbContextOptions SantizeConnectionString(string connectionString)
        {
            var regex = new Regex("metadata.*provider connection string=\\\"(?<connection_string>[^\"]+)\\\"");
            var match = regex.Match(connectionString);
            var constr = match.Success ? match.Groups["connection_string"].Value : connectionString;

            var dbContextOptionsBuilder = new DbContextOptionsBuilder();
            dbContextOptionsBuilder.UseSqlServer(constr);
            return dbContextOptionsBuilder.Options;
        }

        /// <summary>
        ///     This database call opens its own connection and does not participate with other units of work.
        ///     Exceptions are unhandled.
        /// </summary>
        /// <param name="storedProcedureName">The stored procedure to execute.</param>
        /// <param name="parameters">Parameters to include with stored procedure.</param>
        /// <returns>
        ///     0 if the stored procedure does not use the RETURN keyword, and the integer value otherwise.
        ///     Stored procedure must return an integer, otherwise cmd.ExecuteNonQuery() breaks.
        /// </returns>
        public int ExecuteStoredProcedure(string storedProcedureName, SqlParameter[] parameters)
        {
            using (var cmd = Database.GetDbConnection().CreateCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = storedProcedureName;

                if (parameters != null && parameters.Length > 0)
                    cmd.Parameters.AddRange(parameters);

                var returnParameter = new SqlParameter("return", SqlDbType.Int)
                {
                    Direction = ParameterDirection.ReturnValue
                };
                cmd.Parameters.Add(returnParameter);

                try
                {
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                }
                finally
                {
                    cmd.Connection.Close();
                }
                // ReSharper disable once PossibleNullReferenceException
                return Convert.ToInt32((cmd.Parameters["return"] as SqlParameter).Value);
            }
        }

        /// <summary>
        ///     Use this method to read entities which are untracked by the database context.
        ///     This database call opens its own connection and does not participate with other units of work.
        ///     Executes the given stored procedure, and invokes the mapping function on a reader to instantiate
        ///     a list of the given generic type.
        ///     Entities are untracked.
        ///     Exceptions are unhandled.
        /// </summary>
        /// <typeparam name="T">The generic type of the entities to return.</typeparam>
        /// <param name="storedProcedureName">The stored procedure which returns a collection of <typeparamref name="T"/>.</param>
        /// <param name="parameters">Any parameters used by the stored procedure.</param>
        /// <param name="readerMapFunction">
        ///     A function which uses a <see cref="DbDataReader"/> to instantiate an instance of <typeparamref name="T"/>.
        ///     This method invokes this function for each record within the data reader, handling no errors.
        ///     Function must handle errors encountered when reading reader's fields.
        ///     Do not close/modify the reader within the function.
        /// </param>
        /// <returns>List of <typeparamref name="T"/></returns>
        public List<T> ExecuteStoredProcedure<T>(string storedProcedureName, SqlParameter[] parameters, Func<DbDataReader, T> readerMapFunction)
        {
            List<T> result = new List<T>();
            using (var cmd = Database.GetDbConnection().CreateCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = storedProcedureName;

                if (parameters != null && parameters.Length > 0)
                    cmd.Parameters.AddRange(parameters);

                var returnParameter = new SqlParameter("return", SqlDbType.Int)
                {
                    Direction = ParameterDirection.ReturnValue
                };
                cmd.Parameters.Add(returnParameter);

                try
                {
                    cmd.Connection.Open();
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            result.Add(readerMapFunction.Invoke(reader));
                        }
                    }
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }
            return result;
        }
    }
}