﻿namespace Infrastructure.Data.EntityFramework.Interfaces
{
    public interface IParentageState
    {
        void Commit(DbContextCollection dbContextCollection);

        void Dispose(ref bool isCompleted, bool readOnly, DbContextCollection dbContexts);

        void SetAmbientUnitOfWork(UnitOfWork parentUnitOfWork, bool? parentIsDisposed);
    }
}