﻿using Infrastructure.Data.Interfaces;

namespace Infrastructure.Data.EntityFramework.Interfaces
{
    /// <summary>
    ///     Contract for an etity framework core repository.
    /// </summary>
    /// <typeparam name="TEntity">The entity type for the repository.</typeparam>
    public interface IEfRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        /// <summary>
        ///     Connection string for entity framework repository.
        /// </summary>
        string ConnectionString { get; }
    }
}