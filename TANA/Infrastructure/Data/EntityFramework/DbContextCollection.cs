﻿using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Data;
using System.Runtime.ExceptionServices;

namespace Infrastructure.Data.EntityFramework
{
    /// <summary>
    ///     Encapsulates set of pairs of <see cref="DbContextBase" /> and an optional related transaction.
    ///     Provides <see cref="Commit" /> and <see cref="Rollback" /> functionality on the complete set
    ///     (collection).
    /// </summary>
    public class DbContextCollection
    {
        private readonly Dictionary<DbContextBase, IDbContextTransaction> _transactions = new Dictionary<DbContextBase, IDbContextTransaction>();
        private bool _completed = false;
        private bool _disposed = false;
        private IsolationLevel? _isolationLevel;

        private readonly Dictionary<Type, DbContextBase> _initializedDbContexts = new Dictionary<Type, DbContextBase>();

        internal DbContextCollection(IsolationLevel? isolationLevel)
        {
            _isolationLevel = isolationLevel;
        }

        /// <summary>
        ///     Disposes the resources and DbContexts in the collection.
        /// </summary>
        public void Dispose()
        {
            if (_disposed)
            {
                return;
            }

            if (!_completed)
            {
                try
                {
                    Rollback();
                }
                catch (Exception exception)
                {
                    Console.WriteLine("Exception while trying to roll back transactions in a DbContextCollection",
                        exception);
                }
            }

            foreach (var dbContext in _initializedDbContexts.Values)
            {
                try
                {
                    dbContext.Dispose();
                }
                catch (Exception exception)
                {
                    Console.WriteLine("Exception while trying to dispose a DbContext", exception);
                }
            }

            _initializedDbContexts.Clear();
            _disposed = true;
        }

        /// <summary>
        ///     Searches a dictionary collection of <see cref="DbContextBase" /> using type.
        ///     If no such entry exists in the dictionary, an instance is created, added to
        ///     the dictionary and returned.
        ///     Transaction is started if <see cref="_isolationLevel" /> is set.
        ///     This logic ensures there is only 1 instance of a given dbcontext type in this collection.
        /// </summary>
        /// <typeparam name="TDbContext"></typeparam>
        /// <param name="connectionString"></param>
        /// <returns>A dbcontext instance, deriving from <see cref="DbContextBase" />.</returns>
        public DbContextBase Get<TDbContext>(string connectionString) where TDbContext : DbContextBase
        {
            if (_disposed)
            {
                throw new ObjectDisposedException("DbContextCollection");
            }

            var requestedType = typeof(TDbContext);

            if (!typeof(DbContextBase).IsAssignableFrom(requestedType))
            {
                throw new NotSupportedException(
                    "DbContextCollection can only handle contexts which derives from DbContextBase");
            }

            if (!_initializedDbContexts.ContainsKey(requestedType))
            {
                // First time we've been asked for this particular context type.
                // Create one, cache it and start its database transaction if needed.
                var dbContext = (TDbContext)Activator.CreateInstance(requestedType, connectionString);

                _initializedDbContexts.Add(requestedType, dbContext);

                if (_isolationLevel.HasValue)
                {
                    var tran = dbContext.Database.BeginTransaction(); //does not take isolation level as an argument
                    _transactions.Add(dbContext, tran);
                }
            }

            return _initializedDbContexts[requestedType];
        }

        /// <summary>
        ///     Commits all operations for the DbContexts in the collection.
        /// </summary>
        internal void Commit()
        {
            if (_disposed)
            {
                throw new ObjectDisposedException("DbContextCollection");
            }
            if (_completed)
            {
                throw new InvalidOperationException(
                    "You can't call Commit() or Rollback() more than once on a DbContextCollection. All the changes in the DbContext instances managed by this collection have already been saved or rollback and all database transactions have been completed and closed. If you wish to make more data changes, create a new DbContextCollection and make your changes there.");
            }

            ExceptionDispatchInfo lastError = null;

            foreach (var dbContext in _initializedDbContexts.Values)
            {
                try
                {
                    dbContext.SaveChanges();

                    // If we've started an explicit database transaction, time to commit it now.
                    var transaction = GetValueOrDefault(_transactions, dbContext);
                    if (transaction != null)
                    {
                        transaction.Commit();
                        transaction.Dispose();
                    }
                }
                catch (Exception e)
                {
                    lastError = ExceptionDispatchInfo.Capture(e);
                }
            }

            _transactions.Clear();
            _completed = true;

            lastError?.Throw(); // Re-throw while maintaining the exception's original stack track
        }

        /// <summary>
        ///     Rolls back all operations for the DbContexts in the collection.
        /// </summary>
        internal void Rollback()
        {
            if (_disposed)
            {
                throw new ObjectDisposedException("DbContextCollection");
            }
            if (_completed)
            {
                throw new InvalidOperationException(
                    "You can't call Commit() or Rollback() more than once on a DbContextCollection. All the changes in the DbContext instances managed by this collection have already been saved or rollback and all database transactions have been completed and closed. If you wish to make more data changes, create a new EfContextCollection and make your changes there.");
            }

            ExceptionDispatchInfo lastError = null;

            foreach (var dbContext in _initializedDbContexts.Values)
            {
                // There's no need to explicitly rollback changes in a DbContext as
                // DbContext doesn't save any changes until its SaveChanges() method is called.
                // So "rolling back" for a DbContext simply means not calling its SaveChanges()
                // method.

                // But if we've started an explicit database transaction, then we must roll it back.
                var transaction = GetValueOrDefault(_transactions, dbContext);
                if (transaction != null)
                {
                    try
                    {
                        transaction.Rollback();
                        transaction.Dispose();
                    }
                    catch (Exception e)
                    {
                        lastError = ExceptionDispatchInfo.Capture(e);
                    }
                }
            }

            _transactions.Clear();
            _completed = true;

            lastError?.Throw(); // Re-throw while maintaining the exception's original stack track
        }

        private static TValue GetValueOrDefault<TKey, TValue>(IDictionary<TKey, TValue> dictionary, TKey key)
        {
            TValue value;
            return dictionary.TryGetValue(key, out value) ? value : default(TValue);
        }
    }
}