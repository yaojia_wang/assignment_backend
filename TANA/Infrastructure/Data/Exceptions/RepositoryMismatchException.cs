﻿using System;

namespace Infrastructure.Data.Exceptions
{
    /// <summary>
    ///     Thrown when repository matching fails. Repositories are matched when creating unit of work instances in <see cref="UnitOfWorkService"/>.
    ///     Eg. Repositories match when their connection strings are the same.
    /// </summary>
    public class RepositoryMismatchException : Exception
    {
        public RepositoryMismatchException()
        {
        }

        public RepositoryMismatchException(string message)
            : base(message)
        {
        }

        public RepositoryMismatchException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}