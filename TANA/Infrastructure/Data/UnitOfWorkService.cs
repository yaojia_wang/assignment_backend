﻿using Infrastructure.Data.Exceptions;
using Infrastructure.Data.Interfaces;
using System;
using System.Data;
using System.Linq;

namespace Infrastructure.Data
{
    /// <summary>
    ///     Implements service for unit of work.
    /// </summary>
    public class UnitOfWorkService : IUnitOfWorkService
    {
        /// <summary>
        ///     Creates a new unit of work for the specified repositories.
        /// </summary>
        /// <param name="repositories">The repositories that will be used.</param>
        /// <returns>
        /// The created unit of work.
        /// </returns>
        /// <exception cref="System.ArgumentException">If no parameters are passed.</exception>
        /// <exception cref="RepositoryMismatchException">Repository mismatch not supported.</exception>
        public IUnitOfWork CreateUnitOfWork(params IUnitOfWorkFactory[] repositories)
        {
            ValidateParameters(repositories);
            return repositories.First().CreateUnitOfWork();
        }

        /// <summary>
        ///     Creates a new unit of work with a specific isolation level for the specified repositories.
        /// </summary>
        /// <param name="isolationLevel">The isolation level to use.</param>
        /// <param name="repositories">The repositories that will be used.</param>
        /// <returns>
        /// The created unit of work.
        /// </returns>
        /// <exception cref="System.ArgumentException">If no parameters are passed.</exception>
        /// <exception cref="RepositoryMismatchException">Repository mismatch not supported.</exception>
        public IUnitOfWork CreateUnitOfWork(IsolationLevel isolationLevel, params IUnitOfWorkFactory[] repositories)
        {
            ValidateParameters(repositories);
            return repositories.First().CreateUnitOfWork(isolationLevel);
        }

        /// <summary>
        ///     Creates a new read-only unit of work for the specified repositories.
        /// </summary>
        /// <param name="repositories">The repositories that will be used.</param>
        /// <returns>
        /// The created unit of work.
        /// </returns>
        /// <exception cref="System.ArgumentException">If no parameters are passed.</exception>
        /// <exception cref="RepositoryMismatchException">Repository mismatch not supported.</exception>
        public IUnitOfWorkReadOnly CreateReadOnlyUnitOfWork(params IUnitOfWorkFactory[] repositories)
        {
            ValidateParameters(repositories);
            return repositories.First().CreateReadOnlyUnitOfWork();
        }

        private void ValidateParameters(params IUnitOfWorkFactory[] repositories)
        {
            if (repositories.Length == 0)
                throw new ArgumentException(nameof(repositories));

            if (!DoRepositoriesMatch(repositories))
                throw new RepositoryMismatchException("Repository mismatch not supported");
        }

        private bool DoRepositoriesMatch(IUnitOfWorkFactory[] repositories)
        {
            if (repositories.Length == 1)
                return true;

            var firstRepository = repositories.First();

            return repositories.All(repository => firstRepository.Equals(repository));
        }
    }
}